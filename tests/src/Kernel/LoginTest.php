<?php

namespace Drupal\Tests\league_oauth_login_slack\Kernel;

use Drupal\Tests\league_oauth_login\Kernel\LoginTestBase;

/**
 * Class LoginTest to test log in.
 *
 * @group league_oauth_login_slack
 */
class LoginTest extends LoginTestBase {

  /**
   * {@inheritdoc}
   */
  protected $providerId = 'slack';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'league_oauth_login_slack',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getProviderPath() {
    return 'https://slack.com/oauth/authorize';
  }

}
